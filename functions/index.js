const functions = require("firebase-functions");
const express = require("express");
const app = express();
const { db } = require("./util/admin");
const FBAuth = require("./util/fbAuth");

const cors = require("cors");

// Automatically allow cross-origin requests
app.use(cors({ origin: true }));

const {
	getAllFunds,
	createNewFund,
	getOneFund,
	subscribeToFund,
	unsubscribeFromFund,
} = require("./handlers/funds");
const {
	signUp,
	login,
	uploadImage,
	addUserDetails,
	getAuthUserDetails,
	addNewsletterSubscription,
} = require("./handlers/users");

//Fund routes
app.get("/funds", getAllFunds);
app.post("/fund", FBAuth, createNewFund);
app.get("/funds/:fundId", getOneFund);
app.post("/subscription/:subscriptionId", FBAuth, unsubscribeFromFund);
app.post("/funds/:fundId/subscribe", FBAuth, subscribeToFund);
// TODO: edit a post

//User routes
app.post("/signup", signUp);
app.post("/login", login);
app.post("/user", FBAuth, addUserDetails);
app.get("/user", FBAuth, getAuthUserDetails);
app.post("/user/image", FBAuth, uploadImage);

//Newsletter
app.post("/newsletter/subscribe", addNewsletterSubscription);
//Fund routes
//TODO: delete a fund
//TODO: Update a fund
//TODO: upload an image

//TODO: get all fundTiers of a fund
//TODO: Add a fundTier
//TODO: delete a fundTier
//TODO: Update a fundTier
//TODO: Subscribe to a fundTier
//TODO: Unsubscribe from a fundTier
//TODO: upload an image

exports.api = functions.region("europe-west1").https.onRequest(app);

/* exports.createNotificationOnComment = functions
	.region("europe-west1")
	.firestore.document("comments/{id}")
	.onCreate((snapshot) => {
		return db
			.doc(`/posts/${snapshot.data().postId}`)
			.get()
			.then((doc) => {
				if (doc.exists && doc.data().username !== snapshot.data().username) {
					return db.doc(`/notifications/${snapshot.id}`).set({
						recipient: doc.data().username,
						sender: snapshot.data().username,
						read: false,
						postId: doc.id,
						type: "subscription",
						createdAt: new Date().toISOString(),
					});
				} else {
					// eslint-disable-next-line consistent-return
					return;
				}
			})
			.catch((err) => console.error(err));
	});

exports.onPostDelete = functions
	.region("europe-west1")
	.firestore.document(`/posts/{postId}`)
	.onDelete((snapshot, context) => {
		const postId = context.params.postId;
		const batch = db.batch();
		return db
			.collection("comments")
			.where("postId", "==", postId)
			.get()
			.then((data) => {
				data.forEach((doc) => {
					batch.delete(db.doc(`/comments/${doc.id}`));
				});
				return db
					.collection("notifications")
					.where("postId", "==", postId)
					.get();
			})
			.then((data) => {
				data.forEach((doc) => {
					batch.delete(db.doc(`/notifications/${doc.id}`));
				});
				return batch.commit();
			})
			.catch((err) => console.error(err));
	});

exports.onUserImageChange = functions
	.region("europe-west1")
	.firestore.document("/users/{userId}")
	.onUpdate((change) => {
		console.log(change.before.data());
		console.log(change.after.data());
		if (change.before.data().imageUrl !== change.after.data().imageUrl) {
			console.log("image has changed");
			const batch = db.batch();
			return db
				.collection("posts")
				.where("username", "==", change.before.data().username)
				.get()
				.then((data) => {
					data.forEach((doc) => {
						const post = db.doc(`/posts/${doc.id}`);
						batch.update(post, { userImage: change.after.data().imageUrl });
					});
					return batch.commit();
				});
		} else return true;
	});
 */
