const { db, admin, config } = require("../util/admin");

const firebase = require("firebase");

firebase.initializeApp(config);

const { uuid } = require("uuidv4");

const {
	validateSignupData,
	validateLoginData,
	validateNewsletterData,
	reduceUserDetails,
} = require("../util/validators");

// eslint-disable-next-line consistent-return
exports.addNewsletterSubscription = (request, response) => {
	const newSubscription = {
		email: request.body.email,
		createdAt: new Date().toISOString(),
		consent: true,
	};

	const { valid, errors } = validateNewsletterData(newSubscription);

	if (!valid) return response.status(400).json(errors);

	let status, error;

	db.doc(`/newsletterSubscriptions/${newSubscription.email}`)
		.get()
		.then((doc) => {
			if (doc.exists) {
				return true;
			} else {
				// If email is not already subscribed, create new user
				return db
					.doc(`/newsletterSubscriptions/${newSubscription.email}`)
					.set(newSubscription);
			}
		})
		.then(() => {
			return response.status(201).json({ email: newSubscription.email });
		})
		.catch((err) => {
			console.error(err);
			return response.status(500).json({ error: err.code });
		});
};

// eslint-disable-next-line consistent-return
exports.signUp = (request, response) => {
	let token, userId;

	const newUser = {
		email: request.body.email,
		password: request.body.password,
		confirmPassword: request.body.confirmPassword,
		username: request.body.username,
	};

	const { valid, errors } = validateSignupData(newUser);

	if (!valid) return response.status(400).json(errors);

	let noImg = "no-img.png";

	db.doc(`/users/${newUser.username}`)
		.get()
		.then((doc) => {
			if (doc.exists) {
				return response
					.status(400)
					.json({ username: "This username is already taken" });
			} else {
				// If username is not taken, create new user
				return firebase
					.auth()
					.createUserWithEmailAndPassword(newUser.email, newUser.password);
			}
		})
		// If successful, database saves credentials and returns new user token
		.then((data) => {
			userId = data.user.uid;
			return data.user.getIdToken();
		})
		.then((idToken) => {
			token = idToken;
			const userCredentials = {
				email: newUser.email,
				username: newUser.username,
				createdAt: new Date().toISOString(),
				imageUrl: `https://firebasestorage.googleapis.com/v0/b/${config.storageBucket}/o/${noImg}?alt=media`,
				userId,
			};
			return db.doc(`/users/${newUser.username}`).set(userCredentials);
		})
		.then(() => {
			return response.status(201).json({ token });
		})
		.catch((err) => {
			console.error(err);
			if (err.code === "auth/email-already-in-use") {
				return response.status(400).json({ email: "Email is already in use" });
			} else {
				return response.status(500).json({ error: err.code });
			}
		});
};

// eslint-disable-next-line consistent-return
exports.login = (request, response) => {
	const user = {
		email: request.body.email,
		password: request.body.password,
	};

	const { valid, errors } = validateLoginData(user);

	if (!valid) return res.status(400).json(errors);

	firebase
		.auth()
		.signInWithEmailAndPassword(user.email, user.password)
		.then((data) => {
			return data.user.getIdToken();
		})
		.then((token) => {
			return response.json({ token });
		})
		.catch((err) => {
			console.error(err);
			if (err.code === "auth/wrong-password") {
				return response
					.status(403)
					.json({ general: "Wrong credentials, please try again" });
			} else {
				return response.status(500).json({ error: err.code });
			}
		});
};

//Get own user details
exports.getAuthUserDetails = (req, res) => {
	let userData = {};
	db.doc(`/users/${req.user.username}`)
		.get()
		.then((doc) => {
			if (doc.exists) {
				userData.credentials = doc.data();
				return db
					.collection("funds")
					.where("fundId", "==", req.user.username)
					.get();
			} else {
				return res.status(403).json({ error: "User doesn't exist" });
			}
		})
		.then((data) => {
			userData.funds = [];
			data.forEach((fund) => {
				userData.funds.push({
					fundId: doc.data().fundId,
					name: doc.data().name,
					createdAt: doc.data().createdAt,
					active: doc.data().active,
					subscriberCount: doc.data().subscriberCount,
					thumbImgUrl: doc.data().thumbImgUrl,
				});
			});
			return res.json(userData);
		})
		.catch((err) => {
			console.error(err);
			return res.status(500).json({ error: err.code });
		});
};

// eslint-disable-next-line consistent-return
exports.addUserDetails = (req, res) => {
	let userDetails = reduceUserDetails(req.body);

	db.doc(`/users/${req.user.username}`)
		.update(userDetails)
		.then(() => {
			return res.json({ message: "Details added successfully" });
		})
		.catch((err) => {
			console.error(err);
			return res.status(500).json({ error: error.code });
		});
};

//Upload a users profile image.
exports.uploadImage = (req, res) => {
	const BusBoy = require("busboy");
	const path = require("path");
	const os = require("os");
	const fs = require("fs");

	const busboy = new BusBoy({ headers: req.headers });

	let imageToBeUploaded = {};
	let imageFileName;
	// String for image token
	let generatedToken = uuid();

	// eslint-disable-next-line consistent-return
	busboy.on("file", (fieldname, file, filename, encoding, mimetype) => {
		//console.log(fieldname, file, filename, mimetype);
		if (mimetype !== "image/jpeg" && mimetype !== "image/png") {
			return res.status(400).json({ error: "Wrong file type submitted" });
		}
		// my.image.png => ['my', 'image', 'png']
		const imageExtension = filename.split(".")[filename.split(".").length - 1];
		// 32756238461724837.png
		imageFileName = `${Math.round(
			Math.random() * 1000000000000
		).toString()}.${imageExtension}`;
		const filepath = path.join(os.tmpdir(), imageFileName);
		imageToBeUploaded = { filepath, mimetype };
		file.pipe(fs.createWriteStream(filepath));
	});
	busboy.on("finish", () => {
		admin
			.storage()
			.bucket()
			.upload(imageToBeUploaded.filepath, {
				resumable: false,
				metadata: {
					metadata: {
						contentType: imageToBeUploaded.mimetype,
						//Generate token to be appended to imageUrl
						firebaseStorageDownloadTokens: generatedToken,
					},
				},
			})
			.then(() => {
				// Append token to url
				const imageUrl = `https://firebasestorage.googleapis.com/v0/b/${config.storageBucket}/o/${imageFileName}?alt=media&token=${generatedToken}`;
				return db.doc(`/users/${req.user.username}`).update({ imageUrl });
			})
			.then(() => {
				return res.json({ message: "image uploaded successfully" });
			})
			.catch((err) => {
				console.error(err);
				return res.status(500).json({ error: "something went wrong" });
			});
	});
	busboy.end(req.rawBody);
};
