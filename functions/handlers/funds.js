const { db } = require("../util/admin");

// eslint-disable-next-line consistent-return
exports.createNewFund = (request, response) => {
	if (request.method !== "POST") {
		return response.status(400).json("Method not allowed");
	}

	const newFund = {
		fundId: request.user.uid,
		username: request.user.username,
		createdAt: new Date().toISOString(),
		name: request.body.name,
		active: request.body.active,
		description: request.body.description,
		coverImageUrl: request.body.coverImageUrl,
		thumbImgUrl: request.body.thumbImgUrl,
	};

	db.collection("funds/")
		.doc(newFund.fundId)
		.set(newFund)
		.then((doc) => {
			return response.json({
				message: `document ${doc.id} created successfully`,
			});
		})
		.catch((err) => {
			console.error(err);
			return response.status(500).json({ error: "something went wrong" });
		});
};

exports.getAllFunds = (request, response) => {
	db.collection("funds")
		.where("active", "==", "true")
		.orderBy("createdAt", "desc")
		.get()
		.then((data) => {
			let funds = [];
			data.forEach((doc) => {
				funds.push({
					fundId: doc.id,
					name: doc.data().name,
					description: doc.data().description,
					createdAt: doc.data().createdAt,
					subscriberCount: 0,
					coverImgUrl: doc.data().coverImgUrl,
					thumbImgUrl: doc.data().thumbImgUrl,
				});
			});
			return response.json(funds);
		})
		.catch((err) => {
			console.error(err);
			return response.status(500).json({ error: err.code });
		});
};

exports.getOneFund = (request, response) => {
	let fundData = {};

	db.doc(`/funds/${request.params.fundId}`)
		.get()
		.then((doc) => {
			if (!doc.exists) {
				return res.status(404).json({ error: "Fund not found" });
			}
			fundData = doc.data();
			fundData.fundId = doc.id;

			return db
				.collection("subscriptions")
				.where("fundId", "==", request.params.fundId)
				.orderBy("amount", "desc")
				.limit(5)
				.get();
		})
		.then((data) => {
			fundData.subscriptions = [];
			data.forEach((subscription) =>
				fundData.subscriptions.push(subscription.data())
			);
			return res.json(fundData);
		})
		.catch((err) => {
			console.error(err);
			return res.status(500).json({ error: err.code });
		});
};

// eslint-disable-next-line consistent-return
exports.subscribeToFund = (request, response) => {
	if (request.method !== "POST") {
		return response.status(400).json("Method not allowed");
	}

	const newSubscription = {
		userId: request.user.uid,
		username: request.user.username,
		fundId: request.params.fundId,
		amount: request.body.amount,
		paymentMethod: request.body.paymentMethod,
		createdAt: new Date().toISOString(),
		active: true,
	};

	console.log(request.user);

	let errorHasHappened = false;
	let error = {};

	db.doc(`/funds/${request.params.fundId}`)
		.get()
		.then((doc) => {
			if (!doc.exists) {
				errorHasHappened = true;
				error.status = 404;
				error.message = "Fund not found";
				throw new Error();
			}
			return doc.ref.update({
				subscriberCount: doc.data().subscriberCount + 1,
			});
		})
		.then(() => {
			return db.collection("subscriptions").add(newSubscription);
		})
		.then(() => {
			console.log("Subscription added successfully");
			return res.json(newSubscription);
		})
		.catch((err) => {
			if (!errorHasHappened) {
				error.status = 500;
				error.message = "Something went wrong";
			}
			console.error(err);
			return res.status(error.status).json({ error: error.message });
		});
};

// Unsubscribe
exports.unsubscribeFromFund = (request, response) => {
	const document = db.doc(`/subscription/${request.params.subscriptionId}`);
	document
		.get()
		.then((doc) => {
			if (!doc.exists) {
				return response.status(404).json({ error: "Subscription not found" });
			}
			if (doc.data().username !== request.user.username) {
				return res.status(403).json({ error: "Unauthorized" });
			} else {
				return document.set({ active: false }, { merge: true });
			}
		})
		.then(() => {
			return response.json({ message: "Unsubscribed successfully" });
		})
		.catch((err) => {
			console.error(err);
			return response.status(500).json({ error: err.code });
		});
};
