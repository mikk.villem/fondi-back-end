const functions = require("firebase-functions");
//const serviceAccount = require("../../keys/serviceAccountKey.json");
//let config = require("../../keys/config.json");
const admin = require("firebase-admin");
let config = functions.config().firebase;
//config.credential = admin.credential.cert(serviceAccount);

admin.initializeApp({ config });

const db = admin.firestore();

module.exports = { admin, db, config };
