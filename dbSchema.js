const db = {
	users: [
		{
			userId: "t9axPpXLjOeGHDwOKkQ1",
			email: "mikk@email.com",
			createdAt: "2019-03-12T10:59:52.789Z",
			imageUrl: "image/oadsvcojs/onsdiojvn",
			firstName: "Mikk",
			lastName: "Villem",
			bio: "xxx",
			location: "Tallinn, Estonia",
			active: "true | false",
		},
	],
	newsletterSubscription: [
		{
			email: "mikk@email.com",
		},
	],
	/* 	posts: [
		{
			username: "user",
			body: "xxx",
			createdAt: "2019-03-15T10:59:52.798Z",
			commentCount: 2,
		},
	],
	comments: [
		{
			username: "user",
			postId: "kdjsfgdksuufhgkdsufky",
			body: "nice one mate!",
			createdAt: "2019-03-15T10:59:52.798Z",
		},
	], */
	/* 	notifications: [
		{
			notificationId: "injoidnfb2345",
			recipient: "user",
			sender: "john",
			read: "true | false",
			postId: "kdjsfgdksuufhgkdsufky",
			type: "comment | subscription | post",
			createdAt: "2019-03-15T10:59:52.798Z",
			read: "true | false",
		},
	], */
	funds: [
		{
			fundId: "t9axPpXLjOeGHDwOKkQ1", //userId
			name: "The Weekly planet",
			createdAt: "2019-03-15T10:59:52.798Z",
			active: "true | false",
			description: "This is a new Fund for a popular podcast",
			subscriberCount: 0,
			coverImageUrl: "image/oadsvcojs/onsdiojvn",
			thumbImageUrl: "image/oadsvcojs/onsdiojvn",
		},
	],
	/* fundTiers: [
		{
			fundTierId: "edopifsn2343",
			name: "Small donor",
			description: "User's who donate 5€ get eternal thanks",
			amount: 5,
			interval: "month",
			imageUrl: "image/oadsvcojs/onsdiojvn",
		},
	], */
	/* 	permissions: [
		{
			userId: "235odfngod",
			username: "Krislin",
			channelId: "iodgvouis45345",
			active: "true | false",
			permissionType: "read | post",
			createdAt: "2019-03-15T10:59:52.798Z",
			changedAt: null,
		},
	], */
	subscriptions: [
		{
			subscriptionId: "iosdng324234",
			fundId: "235234isodnvos",
			userId: "t9axPpXLjOeGHDwOKkQ1",
			username: "mikktrix",
			//fundTierId: "sdogpvjs45345",
			amount: 5,
			paymentMethod: "stripe | paypal | zlick",
			createdAt: "2019-03-15T10:59:52.798Z",
			active: "true | false",
		},
	],
};

// Redux data

const userDetails = {
	credentials: {
		userId: "t9axPpXLjOeGHDwOKkQ1",
		email: "mikk@gmail.com",
		username: "mikktrix",
		createdAt: "2019-03-15T10:59:52.798Z",
		imageUrl: "image/dsfsdkfghskdfgs/dgfdhfgdh",
	},
	funds: [
		{
			fundId: "mikktrix", //userId
			name: "The Weekly planet",
			createdAt: "2019-03-15T10:59:52.798Z",
			active: "true | false",
			description: "This is a new Fund for a popular podcast",
			subscriberCount: 0,
			coverImageUrl: "image/oadsvcojs/onsdiojvn",
			thumbImageUrl: "image/oadsvcojs/onsdiojvn",
		},
	],
};

const fundDetails = {
	credentials: {
		fundId: "t9axPpXLjOeGHDwOKkQ1",
		name: "The Weekly planet",
		subsciptionCount: 0,
		createdAt: "2019-03-15T10:59:52.798Z",
		description: "This is a new Fund for a popular podcast",
		coverImageUrl: "image/oadsvcojs/onsdiojvn",
		thumbImageUrl: "image/oadsvcojs/onsdiojvn",
	},
	subscriptions: [
		{
			subscriptionId: "iosdng324234",
			fundId: "235234isodnvos",
			userId: "t9axPpXLjOeGHDwOKkQ1",
			username: "mikktrix",
			//fundTierId: "sdogpvjs45345",
			amount: 5,
			paymentMethod: "stripe | paypal | zlick",
			createdAt: "2019-03-15T10:59:52.798Z",
			active: "true | false",
		},
	],
};
